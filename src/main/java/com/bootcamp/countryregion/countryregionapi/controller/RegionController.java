package com.bootcamp.countryregion.countryregionapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.countryregion.countryregionapi.model.Region;
import com.bootcamp.countryregion.countryregionapi.service.RegionService;

@RestController
@RequestMapping("/api")
public class RegionController {
    
    @Autowired
    private RegionService regionService;

    @GetMapping("/region-info")
    public Region getRegionInfo(@RequestParam String regionCode){
        return regionService.getRegionByRegionCode(regionCode);
    }
}
